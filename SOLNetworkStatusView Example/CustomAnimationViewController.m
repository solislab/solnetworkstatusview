//
//  CustomAnimationViewController.m
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 6/8/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

//shortcut to app delegate, i usually call it XAppdelegate
#define XAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#import "CustomAnimationViewController.h"
#import "AppDelegate.h"
#import <pop/POP.h>

@interface CustomAnimationViewController () {
    
    SOLNetworkStatusView *networkStatusView;
    
}

@end

@implementation CustomAnimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //if there is no navigation bar, use self.view instead
    networkStatusView = [[SOLNetworkStatusView alloc] initWithView:self.navigationController.view andWindow:XAppDelegate.window];
    networkStatusView.delegate = self;
    
    //don't pull down but dimmed it
    networkStatusView.willLowDownSuperView = NO;
    networkStatusView.willDimmedSuperView = YES;
    
    //initial network view position
    networkStatusView.networkViewPosX = -self.navigationController.view.frame.size.width;
    networkStatusView.networkViewPosY = (self.navigationController.view.frame.size.height - networkStatusView.networkViewHeight)/2;
    
    //define start animation
    POPBasicAnimation *showUpAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
    showUpAnimation.duration = 0.7;
    showUpAnimation.name = NETWORK_ANIMATION_FOR_VIEW;
    showUpAnimation.toValue = [NSValue valueWithCGPoint:self.navigationController.view.center];
    //assign it to network status view
    networkStatusView.showUpAnimation = showUpAnimation;
    
    //define end animation
    POPBasicAnimation *hidingAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionX];
    hidingAnimation.duration = 0.7;
    hidingAnimation.toValue = @(-self.navigationController.view.frame.size.width);
    //assign it to network status view
    networkStatusView.hideAnimation = hidingAnimation;

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //normally we don't need the below line of code but because i need to demo multiple network status views so i need to..
    [networkStatusView stopWatchingNetworkStatus];
    [networkStatusView removeFromSuperview];
}

- (IBAction)showingAction:(id)sender {
    [networkStatusView startShowingNetworkView];
}

- (IBAction)nextDemoAction:(id)sender {
    
}

- (IBAction)previousDemoAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Network Status View Delegate
- (void)netWorkIsWatingForConnection {
    [self.btnNext setEnabled:NO];
    [self.btnPrevious setEnabled:NO];
    [self.btnShow setEnabled:NO];
    
}

- (void)networkDidConnected {
    NSLog(@"network did connect again");
}

- (void)networkDidDisconnected {
    NSLog(@"network is no longer availabel");
}

- (void)networkStatusViewDidComfinishedShowing {
    //reset cornor radius
    [networkStatusView.layer setCornerRadius:0];
    
    [self.btnNext setEnabled:YES];
    [self.btnPrevious setEnabled:YES];
    [self.btnShow setEnabled:YES];
    NSLog(@"network did finish its cycle");
}

@end
