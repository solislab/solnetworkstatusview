//
//  CustomAnimationViewController.h
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 6/8/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLNetworkStatusView.h"

@interface CustomAnimationViewController : UIViewController <SOLNetWorkStatusViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnShow;
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end
