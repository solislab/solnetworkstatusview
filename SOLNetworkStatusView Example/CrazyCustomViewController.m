//
//  CrazyCustomViewController.m
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 6/9/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//
//shortcut to app delegate, i usually call it XAppdelegate
#define XAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#import "CrazyCustomViewController.h"
#import "SOLNetworkStatusView.h"
#import "AppDelegate.h"

@interface CrazyCustomViewController (){
    SOLNetworkStatusView *networkStatusView;
}

@end

@implementation CrazyCustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //if there is no navigation bar, use self.view instead
    networkStatusView = [[SOLNetworkStatusView alloc] initWithView:self.navigationController.view andWindow:XAppDelegate.window];
    networkStatusView.networkViewHeight = self.navigationController.view.frame.size.height;

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //normally we don't need the below line of code but because i need to demo multiple network status views so i need to..
    [networkStatusView stopWatchingNetworkStatus];
    [networkStatusView removeFromSuperview];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (IBAction)tryMeAction:(id)sender {
    [networkStatusView startShowingNetworkView];
}

@end
