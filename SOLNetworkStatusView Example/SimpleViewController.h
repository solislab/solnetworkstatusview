//
//  ViewController.h
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 5/22/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLNetworkStatusView.h"

@interface SimpleViewController : UIViewController <SOLNetWorkStatusViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnForceShowing;
@property (weak, nonatomic) IBOutlet UIButton *btnNextDemo;

@end

