//
//  AnotherSimpleViewController.m
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 6/5/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//
#define XAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#import "AnotherSimpleViewController.h"
#import "AppDelegate.h"

@interface AnotherSimpleViewController () {
    SOLNetworkStatusView *networkStatusView;
}

@end

@implementation AnotherSimpleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    //if there is no navigation bar, use self.view instead
    networkStatusView = [[SOLNetworkStatusView alloc] initWithView:self.navigationController.view andWindow:XAppDelegate.window];
    networkStatusView.delegate = self;
    
    //do some funky color customizations
    networkStatusView.noConnectionBKColor = [UIColor brownColor];
    networkStatusView.waitingForConnectionBKColor = [UIColor purpleColor];
    networkStatusView.internetConnectedBKColor = [UIColor orangeColor];
    
    //do some silly text customizations
    networkStatusView.waitingForConnectionString = @"DK feels hard to breath";
    networkStatusView.internetConnectedString = @"DK feels powerful again";
    networkStatusView.noConnectionString = @"DK needs internet";
    
    //don't pull the view down but dimmed it
    networkStatusView.willDimmedSuperView = YES;
    networkStatusView.willLowDownSuperView = NO;
    
    //a little bouncing up and down
    networkStatusView.willSpringIn = YES;
    networkStatusView.springBounciness = 15;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    //normally we don't need the below line of code but because i need to demo multiple network status views so i need to..
    [networkStatusView stopWatchingNetworkStatus];
    [networkStatusView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (IBAction)startShowingAction:(id)sender {
    [_btnStart setEnabled:NO];
    [networkStatusView startShowingNetworkView];
}

- (IBAction)nextDemoAction:(id)sender {
    [self performSegueWithIdentifier:@"showCustomAnimation" sender:self];
}

- (IBAction)previousDemo:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Network Status View Delegate
- (void)netWorkIsWatingForConnection {
    NSLog(@"network is Waiting for connection");
}

- (void)networkDidConnected {
    NSLog(@"network did connect again");
}

- (void)networkDidDisconnected {
    NSLog(@"network is no longer availabel");
}

- (void)networkStatusViewDidComfinishedShowing {
    [_btnStart setEnabled:YES];
    [_btnNextDemo setEnabled:YES];
    NSLog(@"network did finish its cycle");
}


@end
