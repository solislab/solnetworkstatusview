//
//  SOLNetworkStatusView.m
//  Marker
//
//  Created by quang.tran on 3/27/15.
//  Copyright (c) 2015 Solis Lab. All rights reserved.
//

/*
Version: 1.0

Disclaimer: IMPORTANT:  This software is supplied to you by Solis Lab Solution Co., Ltd.
("Solis Lab Solution") in consideration of your agreement to the following terms, and your
use, installation, modification or redistribution of this Solis Lab Solution software
constitutes acceptance of these terms.  If you do not agree with these terms,
please do not use, install, modify or redistribute this Solis Lab Solution software.

In consideration of your agreement to abide by the following terms, and subject
to these terms, Solis Lab Solution grants you a personal, non-exclusive license, under
Apple's copyrights in this original Solis Lab Solution software (the "Solis Lab Solution Software"), to
use, reproduce, modify and redistribute theSolis Lab Solution Software, with or without
modifications, in source and/or binary forms; provided that if you redistribute
the Solis Lab Solution Software in its entirety and without modifications, you must retain
this notice and the following text and disclaimers in all such redistributions
of the Solis Lab Solution Software.
Neither the name, trademarks, service marks or logos of Solis Lab Solution Co., Ltd. may be used
to endorse or promote products derived from the Solis Lab Solution Software without specific
prior written permission from Solis Lab Solution.  Except as expressly stated in this notice,
no other rights or licenses, express or implied, are granted by Solis Lab Solution herein,
including but not limited to any patent rights that may be infringed by your
derivative works or by other works in which the Solis Lab Solution Software may be
incorporated.

The Solis Lab Solution Software is provided by Solis Lab Solution on an "AS IS" basis.  Solis Lab Solution MAKES NO
WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE, REGARDING THE Solis Lab Solution0 SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
COMBINATION WITH YOUR PRODUCTS.

IN NO EVENT SHALL Solis Lab Solution BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                       GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
DISTRIBUTION OF THE Solis Lab Solution SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
Solis Lab Solution HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Copyright (C) 2015 Solis Lab Solution Co., Ltd. All Rights Reserved.

*/

#import "SOLNetworkStatusView.h"

#define FONT_LIGHT      @"OpenSans-Light"
#define FONT_REGULAR    @"OpenSans"
#define FONT_SIZE       15
#define ANIMATION_DELAY 0.3


@implementation SOLNetworkStatusView {
    
    BOOL isAlreadyNotified;
    BOOL isAlreadyAdded;
    BOOL didCustomized;
    float viewControllerOriginalY;
    UIColor *viewControllerOriginalColor;
    UIView* dimmedView;
    
}

- (void)awakeFromNib {
    
    [self initReachability];
    
    //observing for connection's status changes
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    
}


#pragma mark - Public Methods 
- (instancetype)initWithView:(UIView *)view andWindow:(UIWindow*)window{
    self = [super init];
    if (self) {
        self = [self initWithView:view];
        self.window = window;
        self.willLowDownSuperView = YES;
    }
    return self;
}

- (instancetype)initWithView:(UIView *)view {
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"SOLNetworkStatusView" owner:self options:nil] firstObject];
        self.superView = view;
        self.spaceBetweenLabelAndTop = 0;
        viewControllerOriginalColor = self.superView.backgroundColor;
    }
    
    return self;
}

#pragma mark - Network Observer
- (void)reachabilityDidChange:(NSNotification *)notification {
    if (isAlreadyNotified == NO) {
        isAlreadyNotified = YES;
        [self performSelector:@selector(startShowingNetworkView) withObject:nil afterDelay:0.3];
    }
}

#pragma mark - UI State

//start checking internet and showing animation
- (void)startShowingNetworkView {
    
    //prevent duplicate notification
    UINavigationController *navigationController = (UINavigationController*)self.window.rootViewController;
    
    if ([[self visibleViewController:navigationController].view isEqual:self.superView] == YES) {
        
        if (didCustomized == NO) {
              [self customUI];
        }
        
        //if network view is already show, don't adding it anymore
        if (isAlreadyAdded == NO) {
            isAlreadyAdded = YES;
            [self addNetWorkStatusViewToViewController];
        }
        
        [self showWaitingState];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [NSTimer scheduledTimerWithTimeInterval:[self getWaitingForConnectionTime] target:self selector:@selector(finalTheState) userInfo:nil repeats:NO];
        });
    }
    
}


//deciding whether internet is connected or disconnected
- (void)finalTheState {
    isAlreadyNotified = NO;
    if ([SOLNetworkStatusView isNetworkReachable]) {
        [self showConnectedState];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //pull back network view
            [NSTimer scheduledTimerWithTimeInterval:[self getDelayRemoveTime] target:self selector:@selector(removeFromViewController) userInfo:nil repeats:NO];
        });
    } else {
        [self showNoInternetState];
    }
}

//change view to waiting state
- (void)showWaitingState {
    _loadingIndicator.hidden = NO;
    [_loadingIndicator startAnimating];
    [_statusLabel setText:[self getWaitingForConnectionString]];
    [self setBackgroundColor:[self getWaitingForConnectionBKColor]];
}

//change view to no internet state
- (void)showNoInternetState {
    
    [_statusLabel setText:[self getNoConnectionString]];
    _loadingIndicator.hidden = YES;
    [self setBackgroundColor:[self getNoConnectionBKColor]];
    if (_delegate) {
        [_delegate networkDidDisconnected];
    }
    if (_delegate && [_delegate respondsToSelector:@selector(networkStatusViewDidComfinishedShowing)]) {
        [_delegate networkStatusViewDidComfinishedShowing];
    }

}

//change view to connected state
- (void)showConnectedState {
    [_loadingIndicator stopAnimating];
    [_statusLabel setText:[self getInternetConnectedString]];
    _loadingIndicator.hidden = YES;
    [self setBackgroundColor:[self getInternetConnectedBKColor]];
    if (self.delegate) {
        [self.delegate networkDidConnected];
    }
}

#pragma mark - Manual Adding - Remove Methods
- (void)addNetWorkStatusViewToViewController {
    
    [self.window addSubview:self];
    
    //Animation for self
    if (self.willSpringIn) {
        [self.layer pop_addAnimation:[self getSpringPullDownAnimation] forKey:@"showUpAnimationWithSpring"];
    } else {
        POPAnimation *showUpAnimation = [self getShowUpAnimation];
        [showUpAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            if (self.delegate) {
                [self.delegate netWorkIsWatingForConnection];
            }
        }];
        if ([[self getShowUpAnimation].name isEqualToString:NETWORK_ANIMATION_FOR_VIEW]) {
            [self pop_addAnimation:showUpAnimation forKey:@"showUpAnimation"];
        } else {
            [self.layer pop_addAnimation:showUpAnimation forKey:@"showUpAnimation"];
        }
    }
    
    //Dimmed Animation
    if (self.willDimmedSuperView) {
        [self createDimmedAnimation];
    }
    
    //Animation for superview
    if (self.willLowDownSuperView) {
        [self.superView.layer pop_addAnimation:[self getBasicPullDowmAnimationForSuperView] forKey:@"pullDownAnimationForSuperView"];
    }
}


- (void)removeFromViewController {
    
    /* Animation for view */
    if (self.willSpringOut) {
        [self.layer pop_addAnimation:[self getSpringPullUpAnimation] forKey:@"pullUpAnimationWithSpring"];
    } else {
        POPAnimation *hideAnimation = [self getHideAnimation];
        [hideAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            isAlreadyAdded = NO;
            [self removeFromSuperview];
            if (_delegate && [_delegate respondsToSelector:@selector(networkStatusViewDidComfinishedShowing)]) {
                [_delegate networkStatusViewDidComfinishedShowing];
            }
        }];
        if ([[self getHideAnimation].name isEqualToString:NETWORK_ANIMATION_FOR_VIEW]) {
            [self pop_addAnimation:hideAnimation forKey:@"pullUpAnimationForSelf"];
        } else {
            [self.layer pop_addAnimation:hideAnimation forKey:@"pullUpAnimationForSelf"];
        }
    }
    
    //Dimmed Animation
    if (self.willDimmedSuperView) {
        [self removeDimmedAnimation];
    }
    
    //Animation for superview
    if (self.willLowDownSuperView) {
        [self.superView.layer pop_addAnimation:[self getBasicPullUpAnimationForSuperView] forKey:@"pullUpAnimationForSuperView"];
    }
}

#pragma mark - Reachability
- (void)initReachability {
    if (_reachability == nil) {
        _reachability = [Reachability reachabilityForInternetConnection];
        [_reachability startNotifier];
    }
}

//checking internet connection by going to google.com
+ (BOOL)isNetworkReachable {

    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com"];
    
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data)
        return YES;
    else
        return NO;
}

//remove observing for network changes
- (void)stopWatchingNetworkStatus {
     [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}


//checking what kind of view controller is showing
#pragma mark - View Controller Helpers
- (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
}

#pragma mark - Customization Method

- (void)customUI {
    
    didCustomized = YES;
    
    _spaceBetweenLabelAndLoadingIndicator.constant = [self getSpaceBetweenLabelAndIndicator];
    _constraintTopSpace.constant = [self getSpaceBetweenLabelAndTop];

    UIFont *newFont = [UIFont fontWithName:[self getLabelFontName] size:[self getFontSize]];
    if (newFont) {
        [_statusLabel setFont:newFont];
    } else {
        [_statusLabel setFont:[UIFont systemFontOfSize:[self getFontSize]]];
    }
    
    [_statusLabel setTextColor:[self getLabelColor]];
    _statusLabel.numberOfLines = 1;
    _statusLabel.adjustsFontSizeToFitWidth = YES;
    [_loadingIndicator hidesWhenStopped];
    
    self.frame = CGRectMake([self getNetworkViewPosX],[self getNetworkViewPosY], [self getNetworkViewWidth], [self getNetworkViewHeight]);

}

#pragma mark - Getters & Setters

/* Font */
- (NSString*)getLabelFontName {
    if (self.fontName.length == 0) {
        self.fontName = FONT_REGULAR;
    }
    
    return self.fontName;
}

- (float)getFontSize {
    if (self.fontSize == 0) {
        self.fontSize = FONT_SIZE;
    }
    
    return self.fontSize;
}


/* Color */
- (UIColor*)getNoConnectionBKColor {
    if (self.noConnectionBKColor == nil) {
        self.noConnectionBKColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1];
    }
    return self.noConnectionBKColor;
}

- (UIColor*)getInternetConnectedBKColor {
    if (self.internetConnectedBKColor == nil) {
        self.internetConnectedBKColor = [UIColor colorWithRed:50/255.0 green:205/255.0 blue:50/255.0 alpha:1];
    }
    return self.internetConnectedBKColor;
}

- (UIColor*)getWaitingForConnectionBKColor {
    if (self.waitingForConnectionBKColor == nil) {
        self.waitingForConnectionBKColor = [UIColor colorWithRed:255/255.0 green:215/255.0 blue:0/255.0 alpha:1];
    }
    return self.waitingForConnectionBKColor;
}

- (UIColor*)getLabelColor {
    if (self.labelColor == nil) {
        self.labelColor = [UIColor whiteColor];
    }
    
    return self.labelColor;
}

/* String */
- (NSString*)getNoConnectionString {
    if (self.noConnectionString.length == 0) {
        self.noConnectionString = @"No Connection";
    }
    
    return self.noConnectionString;
}

- (NSString*)getInternetConnectedString {
    if (self.internetConnectedString.length == 0) {
        self.internetConnectedString = @"Connected";
    }
    
    return self.internetConnectedString;
}

- (NSString*)getWaitingForConnectionString {
    if (self.waitingForConnectionString.length == 0) {
        self.waitingForConnectionString = @"Connecting";
    }
    
    return self.waitingForConnectionString;
}

/* Constraints */
- (float)getSpaceBetweenLabelAndIndicator {
    if (self.spacBetweenLabelAndIndicator == 0) {
        self.spacBetweenLabelAndIndicator = 10;
    }
    
    return self.spacBetweenLabelAndIndicator;
}

- (float)getSpaceBetweenLabelAndTop {

    if (self.spaceBetweenLabelAndTop == 0) {
        self.spaceBetweenLabelAndTop = [self getNetworkViewHeight] != 0 ? [self getNetworkViewHeight]/2 : 35;
    }
    
    return self.spaceBetweenLabelAndTop;
}

/* Size */
- (float)getNetworkViewHeight {
    if (self.networkViewHeight == 0) {
        self.networkViewHeight = 60;
    }
    
    return self.networkViewHeight;
}

- (float)getNetworkViewWidth {
    if (self.networkViewWidth == 0) {
        self.networkViewWidth = self.superView.frame.size.width;
    }
    
    return self.networkViewWidth;
}

/* Positioning */
- (float)getNetworkViewPosX {
    if (self.networkViewPosX == 0) {
        self.networkViewPosX = (self.superView.frame.size.width - [self getNetworkViewWidth])/2;
    }
    
    return self.networkViewPosX;
}

- (float)getNetworkViewPosY {
    if (self.networkViewPosY == 0) {
        self.networkViewPosY = -[self getNetworkViewHeight];
    }
    
    return self.networkViewPosY;
}

#pragma mark - Timing
- (float)getWaitingForConnectionTime {
    if (self.waitingForConnectionTime == 0) {
        self.waitingForConnectionTime = 2.0;
    }
    
    return self.waitingForConnectionTime;
}

- (float)getDelayRemoveTime {
    if (self.delayRemoveTime == 0) {
        self.delayRemoveTime = 1.0;
    }
    
    return self.delayRemoveTime;
}

- (float)getNetworkViewShowAnimationDuration {
    if (self.networkViewShowAnimationDuration == 0) {
        self.networkViewShowAnimationDuration = 1.0;
    }
    
    return self.networkViewShowAnimationDuration;
}

- (float)getNetworkViewHideAnimationDuration {
    if (self.networkViewHideAnimationDuration == 0) {
        self.networkViewHideAnimationDuration = 1.0;
    }
    
    return self.networkViewHideAnimationDuration;
}

#pragma mark - Animation
- (POPAnimation*)getShowUpAnimation {
    if (self.showUpAnimation == nil) {
        self.showUpAnimation = [self getBasicPullDownAnimation];
    }
    
    return self.showUpAnimation;
}


- (POPAnimation*)getHideAnimation {
    if (self.hideAnimation == nil) {
        self.hideAnimation = [self getBasicPullUpAnimation];
    }
    
    return self.hideAnimation;
}



- (POPBasicAnimation*)getBasicPullDownAnimation {
    POPBasicAnimation *pullDownAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullDownAnimation.beginTime = CACurrentMediaTime();
    pullDownAnimation.duration = [self getNetworkViewShowAnimationDuration];
    pullDownAnimation.toValue = [NSValue valueWithCGRect:[self getAnimationStartRect]];
    
    return pullDownAnimation;
}

- (POPSpringAnimation*)getSpringPullDownAnimation {
    POPSpringAnimation *pullDownAnimationWithSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullDownAnimationWithSpring.beginTime = CACurrentMediaTime();
    pullDownAnimationWithSpring.springSpeed = [self getNetworkViewShowAnimationDuration];
    pullDownAnimationWithSpring.springBounciness = [self getSpringBounciness];
    [pullDownAnimationWithSpring setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if (self.delegate) {
            [self.delegate netWorkIsWatingForConnection];
        }
    }];
    pullDownAnimationWithSpring.toValue = [NSValue valueWithCGRect:[self getAnimationStartRect]];
    
    return pullDownAnimationWithSpring;
}

- (void)createDimmedAnimation {
    
    dimmedView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.superView.frame.size.width, self.superView.frame.size.height)];
    dimmedView.backgroundColor = [UIColor clearColor];
    
    [self.superView insertSubview:dimmedView belowSubview:self];
    
    POPBasicAnimation *dimmedAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerBackgroundColor];
    dimmedAnimation.beginTime = CACurrentMediaTime();
    dimmedAnimation.duration = [self getNetworkViewShowAnimationDuration];
    [dimmedAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.superView.userInteractionEnabled = NO;
    }];
    dimmedAnimation.toValue = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.75];
    
    [dimmedView.layer pop_addAnimation:dimmedAnimation forKey:@"dimmedAnimation"];
    
}

- (POPBasicAnimation*)getBasicPullDowmAnimationForSuperView {
    
    POPBasicAnimation *pullDownAnimationForSuperView = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullDownAnimationForSuperView.beginTime = CACurrentMediaTime() + ANIMATION_DELAY;
    pullDownAnimationForSuperView.duration = [self getNetworkViewHideAnimationDuration];
    
    CGRect superViewNewFrame = self.superView.frame;
    superViewNewFrame.origin.y = [self getNetworkViewHeight];
    
    pullDownAnimationForSuperView.toValue = [NSValue valueWithCGRect:superViewNewFrame];
    
    return pullDownAnimationForSuperView;

}

- (POPBasicAnimation*)getBasicPullUpAnimation {
    POPBasicAnimation *pullUpAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullUpAnimation.beginTime = CACurrentMediaTime();
    pullUpAnimation.duration = [self getNetworkViewHideAnimationDuration];
    pullUpAnimation.toValue = [NSValue valueWithCGRect:[self getAnimationStopRect]];
    
    return pullUpAnimation;
}

- (POPSpringAnimation*)getSpringPullUpAnimation {
    POPSpringAnimation *pullUpAnimationWithSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullUpAnimationWithSpring.beginTime = CACurrentMediaTime();
    pullUpAnimationWithSpring.springSpeed = [self getNetworkViewShowAnimationDuration];
    pullUpAnimationWithSpring.springBounciness = [self getSpringBounciness];
    [pullUpAnimationWithSpring setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        isAlreadyAdded = NO;
        [self removeFromSuperview];
        if (_delegate && [_delegate respondsToSelector:@selector(networkStatusViewDidComfinishedShowing)]) {
            [_delegate networkStatusViewDidComfinishedShowing];
        }
        
    }];
    pullUpAnimationWithSpring.toValue = [NSValue valueWithCGRect:[self getAnimationStopRect]];
    
    return pullUpAnimationWithSpring;
}

- (void)removeDimmedAnimation {
    POPBasicAnimation *removeDimmedAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerBackgroundColor];
    removeDimmedAnimation.beginTime = CACurrentMediaTime();
    removeDimmedAnimation.duration = [self getNetworkViewShowAnimationDuration];
    [removeDimmedAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [dimmedView removeFromSuperview];
        self.superView.userInteractionEnabled = YES;
    }];
    removeDimmedAnimation.toValue = [UIColor clearColor];
    [dimmedView.layer pop_addAnimation:removeDimmedAnimation forKey:@"removeDimmedAnimation"];
}

- (POPBasicAnimation*)getBasicPullUpAnimationForSuperView {
    POPBasicAnimation *pullUpAnimationForSuperView = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    pullUpAnimationForSuperView.beginTime = CACurrentMediaTime() + ANIMATION_DELAY;
    pullUpAnimationForSuperView.beginTime = [self getNetworkViewHideAnimationDuration];
    CGRect superViewNewFrame = self.superView.frame;
    superViewNewFrame.origin.y = viewControllerOriginalY;
    
    pullUpAnimationForSuperView.toValue = [NSValue valueWithCGRect:superViewNewFrame];
    
    return pullUpAnimationForSuperView;
}



- (CGRect)getAnimationStartRect {
    if (CGRectIsEmpty(self.animationStartRect) ) {
        CGRect viewNewFrame = self.frame;
        viewNewFrame.origin.y = 0;
        self.animationStartRect = viewNewFrame;
    }
    
    return self.animationStartRect;
}

- (CGRect)getAnimationStopRect {
    if (CGRectIsEmpty(self.animationStopRect)) {
        CGRect viewNewFrame = self.frame;
        viewNewFrame.origin.y = [self getNetworkViewPosY];
        viewNewFrame.origin.x = [self getNetworkViewPosX];
        self.animationStopRect = viewNewFrame;
    }
    
    return self.animationStopRect;
}

- (float)getSpringBounciness {
    if (self.springBounciness == 0) {
        self.springBounciness = 4;
    }
    
    return self.springBounciness;
}


@end
