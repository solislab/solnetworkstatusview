//
//  ViewController.m
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 5/22/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#define XAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#import "SimpleViewController.h"
#import "AppDelegate.h"

@interface SimpleViewController () {

    SOLNetworkStatusView *networkStatusView;
    
}

@end

@implementation SimpleViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //if there is no navigation bar, use self.view instead
    networkStatusView = [[SOLNetworkStatusView alloc] initWithView:self.navigationController.view andWindow:XAppDelegate.window];
    networkStatusView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //normally we don't need the below line of code but because i need to demo multiple network status views so i need to..
    [networkStatusView stopWatchingNetworkStatus];
    [networkStatusView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showViewAction:(id)sender {
    [_btnForceShowing setEnabled:NO];
    [_btnNextDemo setEnabled:NO];
    [networkStatusView startShowingNetworkView];
}

- (IBAction)nextDemoView:(id)sender {
    [self performSegueWithIdentifier:@"showHowCustomView" sender:self];
}


#pragma mark - Network Status View Delegate
- (void)netWorkIsWatingForConnection {
    NSLog(@"network is Waiting for connection");
}

- (void)networkDidConnected {
    NSLog(@"network did connect again");
}

- (void)networkDidDisconnected {
    NSLog(@"network is no longer availabel");
}

- (void)networkStatusViewDidComfinishedShowing {
    [_btnForceShowing setEnabled:YES];
    [_btnNextDemo setEnabled:YES];
    NSLog(@"network did finish its cycle");
}


@end
