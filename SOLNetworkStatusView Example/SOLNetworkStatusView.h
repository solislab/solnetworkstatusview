//
//  SOLNetworkStatusView.h
//  Marker
//
//  Created by quang.tran on 3/27/15.
//  Copyright (c) 2015 Solis Lab. All rights reserved.
//

/*
 Version: 1.0
 
 Disclaimer: IMPORTANT:  This software is supplied to you by Solis Lab Solution Co., Ltd.
 ("Solis Lab Solution") in consideration of your agreement to the following terms, and your
 use, installation, modification or redistribution of this Solis Lab Solution software
 constitutes acceptance of these terms.  If you do not agree with these terms,
 please do not use, install, modify or redistribute this Solis Lab Solution software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Solis Lab Solution grants you a personal, non-exclusive license, under
 Apple's copyrights in this original Solis Lab Solution software (the "Solis Lab Solution Software"), to
 use, reproduce, modify and redistribute theSolis Lab Solution Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Solis Lab Solution Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Solis Lab Solution Software.
 Neither the name, trademarks, service marks or logos of Solis Lab Solution Co., Ltd. may be used
 to endorse or promote products derived from the Solis Lab Solution Software without specific
 prior written permission from Solis Lab Solution.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Solis Lab Solution herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Solis Lab Solution Software may be
 incorporated.
 
 The Solis Lab Solution Software is provided by Solis Lab Solution on an "AS IS" basis.  Solis Lab Solution MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE Solis Lab Solution0 SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL Solis Lab Solution BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE Solis Lab Solution SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 Solis Lab Solution HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2015 Solis Lab Solution Co., Ltd. All Rights Reserved.
 
 */

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <pop/POP.h>

#define NETWORK_ANIMATION_FOR_VIEW  @"ANIMATION_FOR_VIEW"
#define NETWORK_ANIMATION_FOR_LAYER @"NETWORK_ANIMATION_FOR_LAYER"

@protocol SOLNetWorkStatusViewDelegate <NSObject>

- (void)networkDidConnected; //get called when internet is availabel again
- (void)networkDidDisconnected; //get called when internet is interrupted
- (void)netWorkIsWatingForConnection; //get called while waiting for connection

@optional

- (void)networkStatusViewDidComfinishedShowing; //get called when finishing a running cycle

@end



@interface SOLNetworkStatusView : UIView <UIViewControllerTransitioningDelegate>

//operate on app's window
@property (strong, nonatomic) UIWindow *window;

//contains delegates method for developers to adapt accordingly
@property (nonatomic) id<SOLNetWorkStatusViewDelegate> delegate;

//use apple reachability for network status
@property (strong, nonatomic) Reachability* reachability;

//constraints spacing between controls, don't use this explicitly, won't have any effects
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spaceBetweenLabelAndLoadingIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopSpace;

//3 elemental controls
@property (strong, nonatomic) UIView *superView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

// add network view to your view with this line of code
- (instancetype)initWithView:(UIView*)view andWindow:(UIWindow*)window;

//force showing network view with this method
- (void)startShowingNetworkView;

//stop observing for network status
- (void)stopWatchingNetworkStatus;

//free useful method to check internet connection
+ (BOOL)isNetworkReachable;

/**** ONLY USING THESE TO CUSTOMIZE ****/
#pragma mark - Font, Color and Size
@property (strong, nonatomic) NSString *fontName; //default is system font
@property (nonatomic) float fontSize; //default font size is 15

@property (strong, nonatomic) UIColor *noConnectionBKColor; //default is red color
@property (strong, nonatomic) UIColor *internetConnectedBKColor; //default is green
@property (strong, nonatomic) UIColor *waitingForConnectionBKColor; //default is yellow
@property (strong, nonatomic) UIColor *labelColor; //default is white

@property float networkViewHeight; //default is 60
@property float networkViewWidth; //default is equal to superview

#pragma mark - Positioning
@property float networkViewPosX; //initial position x
@property float networkViewPosY; //initial position y

@property float spacBetweenLabelAndIndicator; //adjust spacing
@property float spaceBetweenLabelAndTop;

#pragma mark - Text
@property (strong, nonatomic) NSString *noConnectionString;
@property (strong, nonatomic) NSString *internetConnectedString;
@property (strong, nonatomic) NSString *waitingForConnectionString;

#pragma mark - Timing
@property (nonatomic) float waitingForConnectionTime;
@property (nonatomic) float delayRemoveTime;

@property (nonatomic) float networkViewShowAnimationDuration;
@property (nonatomic) float networkViewHideAnimationDuration;

#pragma mark - Animation
@property (nonatomic) BOOL willLowDownSuperView; //default YES
@property (nonatomic) BOOL willDimmedSuperView;

@property (nonatomic) BOOL willSpringIn;
@property (nonatomic) BOOL willSpringOut;

@property (nonatomic) float springBounciness; //default is 4, if willSpringIn/Out is set to yes

//size and position that will be animated to
@property (nonatomic) CGRect animationStartRect;
@property (nonatomic) CGRect animationStopRect;

//how network view will be animated
@property (strong, nonatomic) POPAnimation *showUpAnimation;
@property (strong, nonatomic) POPAnimation *hideAnimation;

@end
