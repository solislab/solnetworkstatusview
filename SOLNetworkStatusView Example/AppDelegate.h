//
//  AppDelegate.h
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 5/22/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

