![alt tag](http://i279.photobucket.com/albums/kk137/heroe2007/demoNetworkGallery.gif)

# **SOLNetworkStatusView** #
SOLNetworkStatusView is a special view that watches for changes in internet connection to inform users and update App UI accordingly in many elegant ways. It is written for developers to quickly integrate into their application within a few lines of code but still allowing fully customizations from font size, color to animations. SOLNetworkStatusView uses Apple Reachability for network's detection accuracy and Facebook Pop Framework for smooth Animations. It is a mature and well-tested library that handles network connection in the Marker App. 


## **Demonstration Video** ##

[![Only 2 minutes](http://i279.photobucket.com/albums/kk137/heroe2007/Screen%20Shot%202015-06-09%20at%204.18.28%20PM.png)](https://drive.google.com/file/d/0B6dJIDXMYk1mOHQyenNlazNDMmM/view?usp=sharing)

## **Installation** ##

1. Install Facebook Pop: 
     Pop is available on CocoaPods. Just add the following to your project Podfile:
     pod 'pop', '~> 1.0'
2. Copy NetworkStatusView folder to your project
3. Include "SOLNetWorkStatusView.h" to where you want to use and you're good to go.

## **Usage** ##
Include this to your view controller header file
```
#!objective-c

#import "SOLNetworkStatusView.h"
```

Put this one line of code in viewDidLoad to inject SOLNetworkStatusView
 
```
#!objective-c

SOLNetworkStatusView *networkStatusView = [[SOLNetworkStatusView alloc] initWithView:yourViewController.view andWindow:yourAppDelegate.window];
```
And That's it. You've done injecting SOLNetworkStatusView to your view. You don't have to do anything else. Your View Controller is already fully functional when there are changes in the internet connection.

## **Useful Methods** ##

```
#!objective-c

//force showing network view with this method
- (void)startShowingNetworkView;

//stop observing for network status
- (void)stopWatchingNetworkStatus;

//free useful method to check internet connection
+ (BOOL)isNetworkReachable;
```
You might want to do this in viewWillAppear to instantly check for connection right from the start:

```
#!objective-c

if([SOLNetworkStatusView isNetworkReachable] == NO) {
    [networkStatusView startShowingNetworkView];
}
```


## **Delegate Methods** ##
There are three delegate methods and one optional delegate for you as a developer to adapt your view and your logic to SOLNetWorkStatusView Accordingly.

```
#!objective-c

- (void)networkDidConnected; //get called when internet is availabel again
- (void)networkDidDisconnected; //get called when internet is interrupted
- (void)netWorkIsWatingForConnection; //get called while waiting for connection

@optional

- (void)networkStatusViewDidComfinishedShowing; //get called when finishing a running cycle
```

## **Customize NetworkStatusView** ##



### Change Font Family and Font Size ###


```
#!objective-c

@property (strong, nonatomic) NSString *fontName; //default is system font
@property (nonatomic) float fontSize; //default font size is 15
```



### Change text appear on network status view ###

```
#!objective-c

@property (strong, nonatomic) NSString *noConnectionString;
@property (strong, nonatomic) NSString *internetConnectedString;
@property (strong, nonatomic) NSString *waitingForConnectionString;
```


### Change color appear on network status view ###


```
#!objective-c

@property (strong, nonatomic) UIColor *noConnectionBKColor; //default is red color
@property (strong, nonatomic) UIColor *internetConnectedBKColor; //default is green
@property (strong, nonatomic) UIColor *waitingForConnectionBKColor; //default is yellow
@property (strong, nonatomic) UIColor *labelColor; //default is white
```

### Positioning Network Status View ###
By default, SOLNetworkStatusView stay on top of your view controller. You can change the initial position of SOLNetworkStatusView by the following property:


```
#!objective-c

@property float networkViewHeight; //default is 60
@property float networkViewWidth; //default is equal to superview

@property float networkViewPosX; //initial position x
@property float networkViewPosY; //initial position y
```

### Positioning location and size of SOLNetworkView when default animation end ###

```
#!objective-c

@property (nonatomic) CGRect animationStartRect;
@property (nonatomic) CGRect animationStopRect;
```


### Adjust constraints between controls inside SOLNetworkStatusView ###

```
#!objective-c

@property float spacBetweenLabelAndIndicator; //adjust spacing
@property float spaceBetweenLabelAndTop;
```

### Change timing and animation duration ###

```
#!objective-c

@property (nonatomic) float waitingForConnectionTime;
@property (nonatomic) float delayRemoveTime;

@property (nonatomic) float networkViewShowAnimationDuration;
@property (nonatomic) float networkViewHideAnimationDuration;
```


### Change Animation Style ###

```
#!objective-c

@property (nonatomic) BOOL willLowDownSuperView; //default YES
@property (nonatomic) BOOL willDimmedSuperView;

@property (nonatomic) BOOL willSpringIn; //default no spring, only slide up and down
@property (nonatomic) BOOL willSpringOut;

@property (nonatomic) float springBounciness; //default is 4, if willSpringIn/Out is set to yes
```

### Change default showing animation and hiding animation to something else ###

```
#!objective-c

@property (strong, nonatomic) POPAnimation *showUpAnimation;
@property (strong, nonatomic) POPAnimation *hideAnimation;
```

## **License** ##

 Version: 1.0
 
 Disclaimer: IMPORTANT:  This software is supplied to you by Solis Lab Solution Co., Ltd.
 ("Solis Lab Solution") in consideration of your agreement to the following terms, and your
 use, installation, modification or redistribution of this Solis Lab Solution software
 constitutes acceptance of these terms.  If you do not agree with these terms,
 please do not use, install, modify or redistribute this Solis Lab Solution software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Solis Lab Solution grants you a personal, non-exclusive license, under
 Solis Lab Solution's copyrights in this original Solis Lab Solution software (the "Solis Lab Solution Software"), to
 use, reproduce, modify and redistribute theSolis Lab Solution Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Solis Lab Solution Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Solis Lab Solution Software.
 Neither the name, trademarks, service marks or logos of Solis Lab Solution Co., Ltd. may be used
 to endorse or promote products derived from the Solis Lab Solution Software without specific
 prior written permission from Solis Lab Solution.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Solis Lab Solution herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Solis Lab Solution Software may be
 incorporated.
 
 The Solis Lab Solution Software is provided by Solis Lab Solution on an "AS IS" basis.  Solis Lab Solution MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE Solis Lab Solution0 SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL Solis Lab Solution BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE Solis Lab Solution SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 Solis Lab Solution HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2015 Solis Lab Solution Co., Ltd. All Rights Reserved.