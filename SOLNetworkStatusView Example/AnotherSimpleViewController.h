//
//  AnotherSimpleViewController.h
//  SOLNetworkStatusView Example
//
//  Created by quang.tran on 6/5/15.
//  Copyright (c) 2015 quang.tran@solislab.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLNetworkStatusView.h"

@interface AnotherSimpleViewController : UIViewController <SOLNetWorkStatusViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnNextDemo;

@end
